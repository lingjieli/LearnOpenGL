#version 330 core
layout(location = 0) in vec3 aPos;//位置变量的属性位置为0
//layout(location = 1) in vec3 aColor;//颜色变量的属性位置值为1
layout(location = 1) in vec2 aTexCoord;//纹理变量的属性位置值为2

out vec2 TexCoord;
uniform float offset;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform mat4 transform;

void main()
{
	gl_Position = transform*vec4(aPos.x, aPos.y, aPos.z, 1.0);
	TexCoord=aTexCoord;
};