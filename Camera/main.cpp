#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include "stb_image.h"
#include "Shader.h"

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void processInput(GLFWwindow *window);

//setting 
const unsigned int SCR_width = 800;
const unsigned int SCR_height = 600;

int main()
{
	//glfw:初始化和配置
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);//非向后兼容

																  //glfw:创建窗口
	GLFWwindow *window = glfwCreateWindow(SCR_width, SCR_height, "LearnOPenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();//释放资源
		return -1;
	}

	glfwMakeContextCurrent(window);//将窗口上下文设置成主上下文
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);//设置窗口尺寸回调函数

	//glad:载入opengl函数指针
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}


	//render:渲染循环
	glEnable(GL_DEPTH_TEST);//开启深度测试(使用z缓冲)
	while (!glfwWindowShouldClose(window))
	{
		//处理输入
		processInput(window);

		//渲染
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);//状态设置：清空屏幕后用这个颜色填充颜色缓冲
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);//状态使用(清除缓冲区的深度，清除缓冲区的颜色)

		glfwSwapBuffers(window);//交换颜色缓冲
		glfwPollEvents();//检测有没有触发事件，并调用回调函数
	}

	glfwTerminate();//释放缓存
	return 0;
}

void framebuffer_size_callback(GLFWwindow * window, int width, int height)
{
	glViewport(0, 0, width, height);//设置视口(显示)大小
}

void processInput(GLFWwindow * window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)//按下esc键
	{
		glfwSetWindowShouldClose(window, true);
	}
}
