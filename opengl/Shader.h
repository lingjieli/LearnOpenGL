#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader
{
public:
	//程序ID
	unsigned int ID;
	//构造
	Shader(const GLchar *vertexPath,const GLchar *fragmentPath);

	//使用/激活程序
	void use();

	//uniform 工具函数
	void setBool(const std::string &name, bool value) const;
	void setInt(const std::string &name, int value) const;
	void setFloat(const std::string &name, float value) const;

	unsigned int getShader();


	~Shader()
	{};

private:
	//辅助函数：检查编译/链接是否出错
	void checkCompileErrors(unsigned int shader, std::string type);
};


#endif // !SHADER_H



