#version 330 core
out vec4 FragColor;
in vec2 TexCoord;

uniform sampler2D texture1;//纹理采样器
uniform sampler2D texture2;


//in vec4 vertexColor; // 从顶点着色器传来的输入变量（名称相同、类型相同）
void main()
{
	FragColor = mix(texture(texture1,TexCoord),texture(texture2,TexCoord),0.5);
	//FragColor=vec4(1.0,0.0,0.0,1.0);
};