#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include "stb_image.h"
#include "Shader.h"

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void processInput(GLFWwindow *window);

//setting 
const unsigned int SCR_width = 800;
const unsigned int SCR_height = 600;

/*const char *vertexShaderSource = 
"#version 330 core\n"
"layout(location = 0) in vec3 aPos;\n"//位置变量的属性位置为0
"layout(location = 1) in vec3 aColor;\n"//颜色变量的属性位置值为1
"out vec3 ourColor;\n"
"void main()\n"
"{\n"
"	gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"	ourColor=aColor;\n"
"}\0";*/

/*const char *fragmentShaderSource =
"#version 330 core\n"
"out vec4 FragColor;\n"
"in vec3 ourColor;\n"
//"in vec4 vertexColor;\n" // 从顶点着色器传来的输入变量（名称相同、类型相同）
"void main()\n"
"{\n"
"	FragColor = vec4(ourColor,1.0);\n"
"}\n";*/

int test()
{
	//glfw:初始化和配置
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);//非向后兼容

	//glfw:创建窗口
	GLFWwindow *window = glfwCreateWindow(SCR_width, SCR_height, "LearnOPenGL", NULL, NULL);
	if (window==NULL)
	{
		std::cout<<"Failed to create GLFW window"<<std::endl;
		glfwTerminate();//释放资源
		return -1;
	}

	glfwMakeContextCurrent(window);//将窗口上下文设置成主上下文
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);//设置窗口尺寸回调函数

	//glad:载入opengl函数指针
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout<<"Failed to initialize GLAD"<<std::endl;
		return -1;
	}

	//shader:创建着色器程序
	Shader ourShader("./shader/shader.vs","./shader/shader.fs");
	//vertex shader:编译顶点着色器
	/*unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);//将着色器源码附加到着色器对象上：1：字符串数量
	glCompileShader(vertexShader);

	//检测着色器是否编译成功
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}


	//fragment shader:编译片段着色器
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);

	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	//将着色器对象连接到说色气程序中
	unsigned int shaderProgram;
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);//将编译好的着色器附加到着色器程序上
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);//链接着色器对象

	//检测链接是否成功
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::LINK::LINK_FAILED\n" << infoLog << std::endl;
	}

	//删除着色器对象
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);*/


	//设置顶点数据和配置
	//顶点坐标位置(标准化设备坐标)
	/*float vertices[] = {
		-0.5f,-0.5f,0.0f,//p1(x,y,z(像素在空间中与人眼的距离))
		0.5f,-0.5f,0.0,//p2(x,y,z)
		0.0f,0.5f,0.0f,//p3(x,y,z)
	};*/
	float vertices[] = {
		//位置				//颜色
		-0.5f,-0.5f,0.0f,1.0f,0.0f,0.0f,//左下
		0.5f,-0.5f,0.0,0.0f,1.0f,0.0f,//右下
		0.0f,0.5f,0.0f,0.0f,0.0f,1.0f//上
	};

	/*float vertices[] = {
		//     ---- 位置 ----       ---- 颜色 ----     - 纹理坐标 -
		0.5f,  0.5f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f,   // 右上
		0.5f, -0.5f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f,   // 右下
		-0.5f, -0.5f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f,   // 左下
		-0.5f,  0.5f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f    // 左上
	};*/

	//创建纹理对象
	/*unsigned int texture1, texture2;
	glGenTextures(1, &texture1);
	glBindTexture(GL_TEXTURE_2D, texture1);//绑定到二维纹理上
	//设置环绕、过滤方式
	// 为当前绑定的纹理对象设置环绕、过滤方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//载入纹理
	int width, height, nrChannels;
	unsigned char *data = stbi_load("container.jpg", &width, &height, &nrChannels, 0);
	if (data)
	{
		//生成纹理
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);//生成多级渐进纹理
	}
	else
	{
		std::cout << "Failed to load texture1" << std::endl;
	}

	//释放内存
	stbi_image_free(data);

	glGenTextures(1, &texture2);
	glBindTexture(GL_TEXTURE_2D, texture2);//绑定到二维纹理上
										   //设置环绕、过滤方式
										   // 为当前绑定的纹理对象设置环绕、过滤方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	data = stbi_load("awesomeface.png", &width, &height, &nrChannels, 0);
	if (data)
	{
		//生成纹理
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);//生成多级渐进纹理
	}
	else
	{
		std::cout << "Failed to load texture2" << std::endl;
	}
	//释放内存
	stbi_image_free(data);*/

	//创建顶点缓冲对象(VBO)，管理显存
	unsigned int VBO;
	unsigned int VAO;//顶点缓冲对象,保存顶点配置  
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);//绑定到顶点缓冲类型上
	//配置当前缓冲
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);//复制数据到缓冲的内存中，显卡管理数据给定数据的方式(GL_STATIC_DRAW,数据几乎不会改变)

	//链接顶点属性
	//位置属性
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);//设置如何解释顶点数据，
	glEnableVertexAttribArray(0);//将这批顶点数据标志设置为0

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	/*glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(2);*/

	//解绑
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	//设置fragment纹理采样器的纹理
	/*ourShader.use();
	glUniform1i(glGetUniformLocation(ourShader.ID, "texture1"), 0);
	ourShader.setInt("texture2", 1);*/

	//render:渲染循环
	while (!glfwWindowShouldClose(window))
	{
		//处理输入
		processInput(window);

		//渲染
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);//状态设置：清空屏幕后用这个颜色填充颜色缓冲
		glClear(GL_COLOR_BUFFER_BIT);//状态使用

		//绘制
		//glUseProgram(shaderProgram);//激活着色器
		/*float timeValue = glfwGetTime();
		float greenValue = (sin(timeValue) / 2.0f) + 0.5f;
		int vertexColorLocation = glGetUniformLocation(shaderProgram, "ourColor");
		if (vertexColorLocation == -1)
		{
			std::cout << "ERROR::getUniform::FAILED" << std::endl;
		}
		else
		{
			glUniform4f(vertexColorLocation, 0.0f, greenValue, 0.0, 0.0);
		}*/

		//ourShader.setFloat("offset", 0.5);
		//绑定对应的纹理到纹理单元
		/*glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);*/

		ourShader.use();
		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_INT,0);


		glfwSwapBuffers(window);//交换颜色缓冲
		glfwPollEvents();//检测有没有触发事件，并调用回调函数
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);


	glfwTerminate();//释放缓存
	return 0;
}

void framebuffer_size_callback(GLFWwindow * window, int width, int height)
{
	glViewport(0, 0, width, height);//设置视口(显示)大小
}

void processInput(GLFWwindow * window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)//按下esc键
	{
		glfwSetWindowShouldClose(window,true);
	}
}
